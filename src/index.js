/* Import assets */
import './assets/css/toolkit.css';
import './assets/css/additional-styles.css';
import './assets/images/sky.png';

/* Import supporting utility files */
import CurrentUser from './lib/CurrentUser';

/* Import React tooling and source files */
import React from 'react';
import ReactDOM from 'react-dom';
import {
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom';
import Home from './pages/Home';
import Browse from './pages/Browse';
import Profile from './pages/Profile';
import ProfileEdit from './pages/ProfileEdit';
import Login from './pages/Login';

const currentUser = new CurrentUser()

const AvatarInitials = () => {
  if (currentUser.isLoggedIn()) {
    return (
      <li>
        <Link to={'/profile'} className="nav-bar-avatar">
          <span class="avatar-initials ">
            <span class="avatar-initials-text">
              {currentUser.initials()}
            </span>
          </span>
        </Link>
      </li>
    )
  } else {
    return (
      <li>
        <Link to={'/login'}>
          Login
        </Link>
      </li>
    )
  }
}

ReactDOM.render(
  <Router>
    <div>
      <nav class="top-bar nav-bar is-fixed">
    	  <div class="top-bar-title nav-logo">
    	    <Link to={'/'}>
    	      <span class="nav-bar-logo">
    	      <span class="ui-icon i-white i-base">
    	        <svg dangerouslySetInnerHTML={{__html: "<use xlink:href=\"#i-ls-logo\"></use>"}} />
    	      </span>      </span>
    	      Life Sciences
    	    </Link>
    	  </div>

    	  <div class="top-bar-left">
    	    <nav class="main-navigation">
    	      <ul class="menu">
    	          <li id="nav-item-browse" class="  ">
    	            <Link to={'/browse'}>
    	            <span>Browse</span>
    	            </Link>
    	          </li>      </ul>
    	    </nav>
    	  </div>

    	  <div class="top-bar-right">
    	    <nav class="secondary-navigation">
    	      <ul class="menu">
    	        <li>
    	          <a href="#" class="nav-bar-icon">
    	           <span class="ui-icon i-primary i-base ">
    	             <svg dangerouslySetInnerHTML={{__html: "<use xlink:href=\"#i-help\"></use>"}} />
    	           </span>          </a>
    	        </li>
    	        <AvatarInitials />
    	        <li>
    	          <a class="nav-mobile-toggler">
    	            <span class="ui-icon i-primary i-medium icon-open">
                    <svg dangerouslySetInnerHTML={{__html: "<use xlink:href=\"#i-menu\"></use>"}} />
    	            </span>            <span class="ui-icon i-primary i-medium icon-close">

                    <svg dangerouslySetInnerHTML={{__html: "<use xlink:href=\"#i-close\"></use>"}} />
    	            </span>          </a>
    	        </li>
    	      </ul>

    	    </nav>

    	  </div>
    	</nav>

      <Route exact path="/" component={Home}/>
      <Route path="/browse" component={Browse}/>
      <Route exact path="/profile" component={Profile}/>
      <Route exact path="/profile/edit" component={ProfileEdit}/>
      <Route path="/login" component={Login}/>
    </div>
  </Router>
  , document.getElementById('root')
);
