export default class {
  constructor() {
    if (localStorage.getItem('currentUser')) {
      this.userData = JSON.parse(localStorage.getItem('currentUser'))
    } else {
      this.userData = null
    }
  }

  login(data) {
    localStorage.setItem('currentUser', JSON.stringify(data))
  }

  logout() {
    localStorage.removeItem('currentUser')
  }

  isLoggedIn() {
    return this.userData != null
  }

  name() {
    if (this.isLoggedIn()) {
      return `${this.userData.first_name} ${this.userData.last_name}`
    } else {
      return null
    }
  }

  firstName() {
    if (this.isLoggedIn()) {
      return this.userData.first_name
    } else {
      return null
    }
  }

  lastName() {
    if (this.isLoggedIn()) {
      return this.userData.last_name
    } else {
      return null
    }
  }

  initials() {
    if (this.isLoggedIn()) {
      return this.userData.first_name[0] + this.userData.last_name[0]
    } else {
      return null
    }
  }

  email() {
    if (this.isLoggedIn()) {
      return this.userData.email
    } else {
      return null
    }
  }
}
