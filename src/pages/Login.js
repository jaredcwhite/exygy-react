import CurrentUser from '../lib/CurrentUser';
import React, { Component } from 'react';
import { Link } from 'react-router-dom';

const $ = window.$;
const currentUser = new CurrentUser()

export default class extends Component {
  constructor(props) {
    super(props)

    this.state = {
      errorMessage: null
    }
  }

  render() {
    return (
      <div class="page" data-page="login">
        <div class="layout-content">
          <div class="layout-main">
            <div class="page-header">
              <div class="columns">
                <div class="login-box ">
                  <span class="login-box-symbol">
                    <span class="ui-icon i-darkest-blue i-xxlarge ">
                      <svg dangerouslySetInnerHTML={{__html: "<use xlink:href=\"#i-profile-large\"></use>"}} />
                    </span>
                  </span>

                  <div class="login-box-body">
                    <div class="login-box-header">
                      <h2 class="login-box-title">
                        Sign In
                      </h2>
                    </div>

                    <form id="login-form" onSubmit={this.handleSubmit.bind(this)} class="login-box-form">
                      <div class="errors">{this.state.errorMessage}</div>

                      <input id="login-username" class="login-box-form-username" type="text" placeholder="Username" required/>
                      <input id="login-password" class="login-box-form-password" type="password" placeholder="Password" required/>

                      <button class="button login-box-form-submit" data-open="" type="submit">
                        Sign In
                      </button>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }

  handleSubmit(event) {
    event.preventDefault()

    const username = $('#login-username').val()
    const password = $('#login-password').val()

    $('#login-form button.login-box-form-submit').prop('disabled', true)

    $.post('https://exygy-challenge-backend.herokuapp.com/users/login', {
      email: username,
      password: password
    }, (data) => {
      $('#login-form button.login-box-form-submit').prop('disabled', false)
      // data returned is a single item array
      currentUser.login(data[0])
      window.location.href = "/profile"
    }).fail(() => {
      // incorrect username/password
      $('#login-form button.login-box-form-submit').prop('disabled', false)
      const errorHTML = <div><span class="label alert">Username or password not found.<br/>Please try again:</span><br/><br/></div>

      this.setState({errorMessage: errorHTML})
    })
  }
}
