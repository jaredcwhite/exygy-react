import React, { Component } from 'react';
import DocumentList from '../components/DocumentList';

const $ = window.$;

class Browse extends Component {
  constructor(props) {
    super(props)
    this.state = {
      documents: [],
      loading: true
    }

    this.searchTimeout = null

    this.loadDocuments()
  }

  componentWillMount() {
    $('#nav-item-browse').addClass('active')
  }

  componentWillUnmount() {
    $('#nav-item-browse').removeClass('active')
  }

  render(){
    return (
      <div class="page" data-page="browse">
        <div class="layout-content">
          <div class="layout-main">
            <div class="browse-search">
              <div class="search-box-container">
                <span class="ui-icon i-darker-gray i-base search-box-search-icon">
                  <svg dangerouslySetInnerHTML={{__html: "<use xlink:href=\"#i-search\"></use>"}} />
                </span>
                <input type="text" id="textInput" class="search-box " placeholder="" onChange={this.handleSearchChange.bind(this)} />
                <span onClick={this.clearSearch.bind(this)} class="ui-icon i-medium-gray i-base search-box-close-icon">
                  <svg dangerouslySetInnerHTML={{__html: "<use xlink:href=\"#i-close-large\"></use>"}} />
                </span>
              </div>
            </div>

            <div class="browse-stage">
              <div class="browse-slat-set">
                <DocumentList documents={this.state.documents} loading={this.state.loading} />
              </div>
            </div>
          </div>
        </div>
      </div>

    );
  }

  loadDocuments(searchText=null) {
    const params = {
      api_key: 123
    }
    if (searchText && searchText.length > 1) {
      params['search'] = searchText
    }

    $.get('https://exygy-challenge-backend.herokuapp.com/documents', params, (data) => {
      this.setState({documents: data, loading: false})
    })
  }

  handleSearchChange(event) {
    const searchText = event.target.value

    clearTimeout(this.searchTimeout)
    this.searchTimeout = setTimeout(() => {
      this.loadDocuments(searchText)
    }, 250)
  }

  clearSearch() {
    $('.search-box').val('')
    this.loadDocuments()
  }
}

export default Browse
