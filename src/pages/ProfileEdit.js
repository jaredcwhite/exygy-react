import CurrentUser from '../lib/CurrentUser';
import React, { Component } from 'react';
import { Link } from 'react-router-dom';

const $ = window.$;
const currentUser = new CurrentUser()

export default class extends Component {
  constructor(props) {
    super(props)

    this.state = {
      errorMessage: null
    }
  }

  componentWillMount() {
    // Requires a user login
    if (!currentUser.isLoggedIn()) {
      window.location.href = "/"
    }
  }

  render() {
    return (
      <div class="page" data-page="login">
        <div class="layout-content">
          <div class="layout-main">
            <div class="page-header">
              <div class="columns">
                <div class="login-box ">
                  <span class="login-box-symbol">
                    <span class="ui-icon i-darkest-blue i-xxlarge ">
                      <svg dangerouslySetInnerHTML={{__html: "<use xlink:href=\"#i-profile-large\"></use>"}} />
                    </span>
                  </span>

                  <div class="login-box-body">
                    <div class="login-box-header">
                      <h2 class="login-box-title">
                        Edit Profile
                      </h2>
                    </div>

                    <form id="login-form" onSubmit={this.handleSubmit.bind(this)} class="login-box-form">
                      <div class="errors">{this.state.errorMessage}</div>

                      <input id="profile-firstname" class="login-box-form-username" type="text" placeholder="First Name" required value={currentUser.firstName()} />
                      <input id="profile-firstname" class="login-box-form-password" type="text" placeholder="Last Name" required value={currentUser.lastName()} />

                      <input id="profile-username" class="login-box-form-username" type="text" placeholder="Username" required value={currentUser.email()} />
                      <input id="profile-password" class="login-box-form-password" type="password" placeholder="Password" required />

                      <button class="button login-box-form-submit" data-open="" type="submit">
                        Update
                      </button>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }

  handleSubmit(event) {
    event.preventDefault()

    // TODO: for another day...
  }
}
