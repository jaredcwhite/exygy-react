import CurrentUser from '../lib/CurrentUser';
import React, { Component } from 'react';
import { Link } from 'react-router-dom';

const $ = window.$;
const currentUser = new CurrentUser()

export default class extends Component {
  componentWillMount() {
    // Requires a user login
    if (!currentUser.isLoggedIn()) {
      window.location.href = "/"
    }
  }

  render() {
    return (
      <div class="page" data-page="profile">
        <div class="layout-content">
          <div class="layout-main">
            <div class="page-header">
              <div class="columns">
                <div class="login-box ">
                  <span class="login-box-symbol">
                    <span class="avatar-initials ">
                      <span class="avatar-initials-text">
                        {currentUser.initials()}
                      </span>
                    </span>
                  </span>

                  <div class="login-box-body">
                    <div class="login-box-header">
                      <h2 class="login-box-title">
                        {currentUser.name()}
                      </h2>
                    </div>

                    <div class="profile-email">{currentUser.email()}</div>

                    <div class="profile-edit"><Link to={'/profile/edit'}>Edit</Link>
                    <br/><br/>
                    <a href="#" onClick={this.handleLogout.bind(this)}>Logout</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }

  handleLogout(event) {
    event.preventDefault()
    currentUser.logout()
    window.location.href = "/"
  }
}
