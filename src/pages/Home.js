import CurrentUser from '../lib/CurrentUser';
import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class Home extends Component {
  render() {
    var greeting;

    const currentUser = new CurrentUser()

    if (currentUser.isLoggedIn()) {
      greeting = `Welcome Back, ${currentUser.firstName()}`
    } else {
      greeting = "Welcome"
    }

    return (
      <div class="page" data-page="home">
        <div class="background-banner"></div>

        <div class="layout-content">
          <div class="layout-main">
            <div class="page-header">
              <div class="columns">
                <h1 class="page-title">{greeting}</h1>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Home
