import React, { Component } from 'react';
import DocumentItem from '../components/DocumentItem';

export default class extends Component {
  render() {
    const renderedDocuments = this.props.documents.map((item) => {
      return (
        <DocumentItem key={item.id} document={item} />
      )
    })

    if (renderedDocuments.length === 0) {
      if (this.props.loading) {
        return <div class="list-message">Loading…</div>
      } else {
        return <div class="list-message">I'm sorry, I couldn't find any documents with that search query.</div>
      }
    } else {
      return <div>{renderedDocuments}</div>
    }
  }
}
