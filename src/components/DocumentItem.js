import React, { Component } from 'react';

export default class extends Component {
  render() {
    const header = this.props.document.name
    const title = this.props.document.title
    const modifiedBy = this.props.document.modified_by
    const status = this.props.document.status[0].toUpperCase() + this.props.document.status.slice(1)
    const country = this.props.document.country

    let lastModified = Date.parse(this.props.document.updated_at)
    lastModified = new Date(lastModified).toLocaleString().split(',')[0]

    // NOTE: we're trusting this input data as its from an internal data source,
    // but if this were user-generated in any way, we'd want to scrub the data
    // first to avoid nasty injections
    const typeIcon = this.props.document.document_type

    return (
      <div id={'document-' + this.props.document.id} href="#" class="slat row align-middle collapse  excel" data-ref="">
        <div class="slat-avatar item-avatar">
          <span class="ui-icon i-excel i-xlarge ">
            <svg dangerouslySetInnerHTML={{__html: "<use xlink:href=\"#i-file-" + typeIcon + "\"></use>"}} />
          </span>  </div>

        <div class="slat-body expand columns">
          <div class="rows">
            <div class="columns small-12">
              <h3 class="slat-header">{header}</h3>
            </div>
          </div>

            <div class="row slat-subtitle">
              <div class="columns small-12">
                <p class="slat-title-wrapper">
                  <span class="slat-subtitle-prefix">Title: </span>
                  <span class="slat-subtitle-text">{title}</span>
                </p>
              </div>
            </div>

          <div class="row show-for-medium">
            <div class="columns small-12 medium-6">
              <p class="slat-attr-wrapper">
                <span class="slat-attr-key">Modified By: </span>
                <span class="slat-attr-value">{modifiedBy}</span>
              </p>
              <p class="slat-attr-wrapper">
                <span class="slat-attr-key">Last Modified: </span>
                <span class="slat-attr-value">{lastModified}</span>
              </p>
            </div>

            <div class="columns small-12 medium-6">
              <p class="slat-attr-wrapper">
                <span class="slat-attr-key">Status: </span>
                <span class="slat-attr-value">{status}</span>
              </p>
              <p class="slat-attr-wrapper">
                <span class="slat-attr-key">{country.match(/,/) ? 'Countries' : 'Country'}: </span>
                <span class="slat-attr-value">{country}</span>
              </p>
            </div>
          </div>
        </div>

        <div class="slat-secondary columns shrink">
          <span class="ui-icon i-darker-gray i-sm-medium slat-secondary-icon">
            <svg dangerouslySetInnerHTML={{__html: "<use xlink:href=\"#i-star\"></use>"}} />
          </span>
          <span class="float-right slat-dropdown">
            <span class="ui-icon i-darker-gray i-sm-medium slat-secondary-more">
              <svg dangerouslySetInnerHTML={{__html: "<use xlink:href=\"#i-more-vert\"></use>"}} />
            </span>

          </span>

        </div>
      </div>
    )
  }
}
