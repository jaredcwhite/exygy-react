# Jared's Notes

* On mobile, the user avatar initials at the top aren't visible in the responsive menu
* I started on a rudimentary Edit Profile screen, but the fields currently aren't editable.
* I didn't see a way to get to the Login screen from the homepage, so I added a link to the nav bar
* The homepage message changes depending on the login state
* I added a Logout link to the Profile page
* Tested in desktop Safari, Firefox, and Chrome
* It's not ideal that the search query is case-sensitive, but that seems to be due using LIKE rather than ILIKE for the query that's in the Rails/SQL code—so I can't resolve this on the client side :)
